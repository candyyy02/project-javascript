//////////////////////////////////////
function createNewUser() {
  let firstName = "";
  let lastName = "";

  while (firstName === "") {
    firstName = prompt("first name");
  }

  while (lastName === "") {
    lastName = prompt("last name");
  }

  const newUser = {
    firstName,
    lastName,
    getLogin() {
      const loginName =
        this.firstName.toLocaleLowerCase()[0] +
        this.lastName.toLocaleLowerCase();
      return loginName;
    },
  };
  return newUser;
}

const user = createNewUser();

console.log(user.getLogin());
