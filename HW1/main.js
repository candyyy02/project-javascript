let userName;
let userAge = null;

do {
  userName = prompt("What's ur name?");
} while (!userName || Number(userName));

do {
  userAge = prompt("How old are u? ");
} while (!userAge || Number.isNaN(+userAge));

console.log(userAge);
console.log(userName);

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  let agreement = confirm("Are you sure you want to continue?");
  if (agreement) {
    alert("Welcome " + userName);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert("Welcome " + userName);
}
