const confirmButton = document.getElementsByClassName('btn')[0];
const upperInput = document.getElementsByClassName('upper__input')[0];
const upperInputBtn = document.getElementById('upper-input__button');
const lowerInput = document.getElementsByClassName('lower__input')[0];
const lowerInputBtn = document.getElementById('lower-input__button');
upperInputBtn.name = 'upperInputBtn';
lowerInputBtn.name = 'lowerInputBtn';

function toggle(e) {
  if (e.target.classList.contains('fa-eye')) {
    e.target.classList.remove('fa-eye');
    e.target.classList.add('fa-eye-slash');
    e.target.name === 'upperInputBtn'
      ? (upperInput.type = 'password')
      : (lowerInput.type = 'password');
  } else {
    e.target.classList.remove('fa-eye-slash');
    e.target.type = 'text';
    e.target.classList.add('fa-eye');
    e.target.name === 'lowerInputBtn'
      ? (lowerInput.type = 'text')
      : (upperInput.type = 'text');
  }
}

function passwordCheck() {
  if (upperInput.value === lowerInput.value) {
    alert('You are welcome');
  } else {
    lowerInput.insertAdjacentHTML('afterend', '<p style="red">Wrong!</p>');
  }
}

confirmButton.addEventListener('click', passwordCheck);
upperInputBtn.addEventListener('click', toggle);
lowerInputBtn.addEventListener('click', toggle);
