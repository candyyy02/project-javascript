const arryText = [
  'Echo',
  ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 'sea', 'user', 23],
  'Apollo',
  ['1', '2', '3', 'sea', 'user', 23],
  'Odessa',
  'Lviv',
  ['Kharkiv', 'Kiev', ['Borispol', 'Irpin'], 'Odessa', 'Lviv', 'Dnieper'],
];

function printArray(array) {
  let ulElm = document.createElement('ul');
  let liElm = array.map((item) => {
    if (Array.isArray(item)) {
      return printArray(item);
    } else {
      let li = document.createElement('li');
      li.innerHTML = item;
      return li;
    }
  });
  liElm.forEach((item) => {
    ulElm.appendChild(item);
  });
  return ulElm;
}
window.onload = function () {
  viewArray(arryText);
};

function viewArray(array) {
  let arrayElement = printArray(array);
  document.body.appendChild(arrayElement);

  let sec = 3;
  let timer = document.createElement('p');
  timer.style.fontSize = '50px';
  timer.style.color = 'red';
  document.body.appendChild(timer);

  let timeOut = setInterval(function () {
    if (sec < 0) {
      arrayElement.remove();
      timer.remove();
      clearInterval(timeOut);
    } else {
      timer.innerHTML = sec.toString();
      sec--;
    }
  }, 1000);
}
