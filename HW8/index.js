function enterNumber(number) {
  number = number.replace(',', '.');
  if (isNaN(number) || !number) {
    return false;
  }
  return +number;
}
let inputPrice = document.getElementById('inputPrice');

function createElement(tagName, className, innerHTML = '') {
  let newElement = document.createElement(tagName);
  newElement.classList.add(className);
  newElement.innerHTML = innerHTML;
  return newElement;
}

function putNodeInNode(parentNode, childNode) {
  parentNode.append(childNode);
}

//SPAN

let spanElm = createElement('span', 'under__input', 'Current price: $');
let spanPrice = createElement('span', 'under-input__price');
let spanCross = createElement('span', 'under-input__cross', '&times');

//SPAN.PRICE

let enterTrue = createElement(
  'span',
  'correct__text',
  'Please enter correct price.'
);

inputPrice.addEventListener('focus', function () {
  this.value = '';
  this.style.borderColor = 'green';
  this.style.boxShadow = '0px 0px 8px 0px rgba(0,255,0,1)';
});

inputPrice.addEventListener('blur', function () {
  this.style.boxShadow = '';

  if (spanElm) {
    spanElm.remove();
  }
  if (enterTrue) {
    enterTrue.remove();
  }

  let enterPrice = enterNumber(this.value);

  if (!enterPrice || enterPrice <= 0) {
    this.style.borderColor = 'red';
    putNodeInNode(this.parentNode, enterTrue);
  } else {
    this.style.borderColor = '';
    spanPrice.innerHTML = enterPrice;
    putNodeInNode(spanElm, spanPrice);
    putNodeInNode(spanElm, spanCross);
    document.body.insertBefore(spanElm, this.parentNode);
  }
});

spanCross.addEventListener('mouseover', function () {
  this.style.borderColor = 'red';
  this.style.color = 'red';
  inputPrice.style.color = 'green';
});
spanCross.addEventListener('mouseout', function () {
  this.style.borderColor = '';
  this.style.color = '';
  inputPrice.style.color = '';
});
spanCross.addEventListener('click', function () {
  if (spanElm) {
    spanElm.remove();
    inputPrice.value = '';
    inputPrice.style.color = 'green';
  }
});
