///////////////////////////////////////////////
let num1 = +prompt("Enter first num");
let num2 = +prompt("Enter second num");
let maths = prompt("Mathematical operation");

function calculate(num1, num2, maths) {
  switch (maths) {
    case "*":
      return num1 * num2;
    case "/":
      return num1 / num2;
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    default:
      alert("Error");
  }
}

alert(calculate(num1, num2, maths));
