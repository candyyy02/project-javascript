//////////////////////////////////////
function createNewUser() {
  let firstName = "";
  let lastName = "";
  let birthday = "";
  while (firstName === "") {
    firstName = prompt("first name");
  }

  while (lastName === "") {
    lastName = prompt("last name");
  }

  while (birthday === "") {
    birthday = prompt("ur birthday dd.mm.yy: ");
  }

  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      const loginName =
        this.firstName.toLocaleLowerCase()[0] +
        this.lastName.toLocaleLowerCase();
      return loginName;
    },
    getAge: function () {
      let now = new Date();
      let currentYear = now.getFullYear();

      let inputDate = +this.birthday.substring(0, 2);
      let inputMonth = +this.birthday.substring(3, 5);
      let inputYear = +this.birthday.substring(6, 10);

      let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
      let birthYear = birthDate.getFullYear();
      let age = currentYear - birthYear;
      if (now < new Date(birthDate.setFullYear(currentYear))) {
        age = age - 1;
      }
      return age;
    },
    getPassword: function () {
      const newPasss =
        this.firstName.toLocaleLowerCase()[0] +
        this.lastName.toLocaleLowerCase() +
        this.birthday;
      return newPasss;
    },
  };
  return newUser;
}

const user = createNewUser();
console.log(user.getPassword());
